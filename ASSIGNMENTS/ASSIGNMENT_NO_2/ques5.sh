#!/bin/bash
clear

echo -n "Enter the number : "
read n

i=2
while [ $i -lt $n ] 
do
	if [ `expr $n % $i` -eq 0 ]
	then
		echo "Not Prime"
		exit
	fi
	i=`expr $i + 1`
done

if [ $n -eq  $i ]
then
	echo "Prime"
fi
exit
