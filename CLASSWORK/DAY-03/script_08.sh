#!/bin/bash

# script to print name of an executable files in a cur directory


clear

echo "list of an executable files in cur dir are: "
for filepath in `ls`
do
	if [ -e $filepath -a -x $filepath ]
	then
		echo $filepath
	fi

done

exit


