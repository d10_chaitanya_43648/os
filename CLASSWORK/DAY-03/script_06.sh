#!/bin/bash

# accept filepath from user and check wheather it is valid/invalid
# if filepath is invalid - display message "invalid filepath"
# if filepath is valid:
#	then:
#			if filepath is a regular file -> display its contents (command : cat )
#			if filepath is a directory file -> display its contents (command : ls )
#			otherwise display message "filepath is neither regular file nor directory file"



clear

echo -n "enter filepath	: "
read filepath

if [ -e $filepath ] # if filepath is valid
then

	if [ -f $filepath ] # if filepath is a regular file then display its contents by using "cat" comamnd
	then
		echo "contents of regular file: $filepath are: "
		cat $filepath
	elif [ -d $filepath ] # if filepath is a directory file then display its contents by using "ls" command
	then
		echo "contents of directory file: $filepath are: "
		ls $filepath
	else
		echo "$filepath is neither a regular file nor directory file"
	fi


else
	echo "$filepath is invalid filepath !!!"
fi

exit




