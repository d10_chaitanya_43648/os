#!/bin/bash

# script to print table of a given number by using loop:

clear

echo -n "enter number	: "
read num

i=1
echo "while => table of $num is: "

while [ $i -le 10 ]
do

	res=`expr $num \* $i`
	echo "$res"
	i=`expr $i + 1`	#i++

done

i=1
echo "until => table of $num is: "

until [ $i -gt 10 ]
do

	res=`expr $num \* $i`
	echo "$res"
	i=`expr $i + 1`	#i++

done

echo "for => table of $num is: "
# for loop takes value for loop counter from collection sequentially for each iteration
for i in {1..10..2}
do
	res=`expr $num \* $i`
	echo "$res"
done


exit

