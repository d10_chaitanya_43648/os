#!/bin/bash

# script to calculate area of circle
# area of circle = pi * rad * rad

clear

PI=3.14

echo -n "enter radius	: "
read rad

area=`echo "$PI * $rad * $rad" | bc`
echo "area of circle is	: $area"

exit

